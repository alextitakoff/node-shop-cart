//console.log('== prod seeder');
//var express = require('express');
require('../models/test');
var Product = require('../models/product');
var mongoose = require('mongoose');

//mongoose.connect('localhost:27017/shopping');

var products = [
    new Product({
        imagePath: 'http://expressfactions.com/pics/57751fddb6f6f.jpg',
        title: 'Gothic 2',
        description: 'Действие Готики 2 начинается примерно через три недели после событий оригинальной Готики. Основное сюжетное действие разворачивается на острове Хоринис',
        price: 7
    }),
    new Product({
        imagePath: 'http://expressfactions.com/pics/57751fddb6f6f.jpg',
        title: 'Gothic 3',
        description: 'Действие Готики 2 начинается примерно через три недели после событий оригинальной Готики. Основное сюжетное действие разворачивается на острове Хоринис',
        price: 4
    }),
    new Product({
        imagePath: 'http://expressfactions.com/pics/57751fddb6f6f.jpg',
        title: 'Gothic 2',
        description: 'Действие Готики 2 начинается примерно через три недели после событий оригинальной Готики. Основное сюжетное действие разворачивается на острове Хоринис',
        price: 9
    })
];

done = 0;
//console.log('количество продуктов ' + products.length);

for (var i = 0; i < products.length; i++) {
    //console.log('== запись init');

    products[i].save(function(err, result) {
        //console.log(done);
        //  console.log('запись прошла');
        done++;
        if (done === products.length) {
            exit();
        }
    });

}


function exit() {
    mongoose.disconnect();
}
